package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf(
		"Hello User \n\nUserAgent: %s \nMethod: %s \nUrl: %s \nRequestUri: %s \nProto: %s \nHost: %s \n",
		r.UserAgent(),
		r.Method,
		r.URL,
		r.RequestURI,
		r.Proto,
		r.Host,
	)))
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8088", nil)
}
